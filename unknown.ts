let nietZeker: unknown = 4;
nietZeker = "misschien een string";
nietZeker = false; // Toch een boolean

let ding: unknown = {x: 0};
if (typeof ding === "function") {
  ding();     // without if: COMPILE error: ding is not a function
}
// ding.bar = 100; // COMPILE error: Property 'bar' does not exist
ding= 5;
if (typeof ding === "number") {
  const n: number = ding;
} else{
  console.log("ding is geen number");
}