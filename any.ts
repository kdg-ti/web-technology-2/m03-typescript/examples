let nietZeker: any = 4;
nietZeker = "misschien een string";
nietZeker = false; // Toch een boolean

let ding: any = {x: 0};
ding.foo(); // RUNTIME error: ding.foo is not a function
ding();     // RUNTIME error: ding is not a function
ding.bar = 100;
ding= "hello";
const n: number = ding;